package com.addcel.telecom.pago.conciliacion.servicios.model.vo;

public class PagoServicioVO {	
	
	private long idBitacora;	
	private String clave;
    private String disponible1;
    private String disponible2;
    private String disponible3;
    private String fechaOperacion;
    private String folioCliente;
    private String folioProveedor;
    private String folioTelecomm;
    private String idProductoTerceros;
    private String nombre;
    private String fechaNacimiento;

	private String apellidoPaterno;
    private String apellidoMaterno;
    
    
    private String iva;
    private String mensaje;
    private String operador;
   

	private String precio;
    private String premio;
    private String principalLocal;
    private String principalPesos;
    private String redondeo;
    private String referencia1;
    private String referencia2;
    private String referencia3;
    private String subTotal;
    private String terminal;
    private String tienda;
    private String tipoCambio;
    private String tipoMoneda;
    private String total;
    private String rfc;
    private String calle;
    private String colonia;
    private String ciudad;
    private String estado;
    private String codigoPostal;
    private String telefono;
    
    
    private boolean consultaWS;
    
    /*datos para innsertar en la bitacora*/
    
	private long idUsuario;     
	private String imei;	
	private String software;	
	private String modelo;	
	private String wkey;	
	private int pase;
	private int status;  




	public PagoServicioVO(String clave,String disponible1,String disponible2,String disponible3,String fechaOperacion,String folioCliente,String folioProveedor,String folioTelecomm,String idProductoTerceros,String nombre,String fechaNacimiento,String apellidoPaterno,String apellidoMaterno,String iva,String mensaje,String operador,String precio,String premio,String principalLocal,String principalPesos,String redondeo,String referencia1,String referencia2,String referencia3,String subTotal,String terminal,String tienda,String tipoCambio,String tipoMoneda,String total,String rfc, String calle,String colonia,String ciudad,String estado,String codigoPostal,String telefono) {
    	
    	this.clave = clave;
    	this.disponible1 = disponible1;
    	this.disponible2 = disponible2;
    	this.disponible3 = disponible3;
    	this.fechaOperacion = fechaOperacion;
    	this.folioCliente = folioCliente;
    	this.folioProveedor = folioProveedor;
    	this.folioTelecomm = folioTelecomm;
    	this.idProductoTerceros = idProductoTerceros;
    	this.nombre = nombre;
    	this.fechaNacimiento = fechaNacimiento;
    	this.apellidoPaterno = apellidoPaterno;
    	this.apellidoMaterno = apellidoMaterno;
    	this.iva = iva;
    	this.mensaje = mensaje;
    	this.operador = operador;
    	this.precio = precio;
    	this.premio = premio;
    	this.principalLocal = principalLocal;
    	this.principalPesos = principalPesos;
    	this.redondeo = redondeo;
    	this.referencia1 = referencia1;
    	this.referencia2 = referencia2;
    	this.referencia3 = referencia3;
    	this.subTotal = subTotal;
    	this.terminal = terminal;
    	this.tienda = tienda;
    	this.tipoCambio = tipoCambio;
    	this.tipoMoneda = tipoMoneda;
    	this.total = total;
    	this.rfc = rfc; 
    	this.calle = calle;
    	this.colonia = colonia;
    	this.ciudad = ciudad;
    	this.estado = estado;
    	this.codigoPostal = codigoPostal;
    	this.telefono = telefono;
    	
    	
    }
	
	
	public PagoServicioVO(String clave,String disponible1,String disponible2,String disponible3,String fechaOperacion,String folioCliente,String folioProveedor,String folioTelecomm,String idProductoTerceros,String iva,String mensaje,String operador,String precio,String premio,String principalLocal,String principalPesos,String redondeo,String referencia1,String referencia2,String referencia3,String subTotal,String terminal,String tienda,String tipoCambio,String tipoMoneda,String total) {
    	
    	this.clave = clave;
    	this.disponible1 = disponible1;
    	this.disponible2 = disponible2;
    	this.disponible3 = disponible3;
    	this.fechaOperacion = fechaOperacion;
    	this.folioCliente = folioCliente;
    	this.folioProveedor = folioProveedor;
    	this.folioTelecomm = folioTelecomm;
    	this.idProductoTerceros = idProductoTerceros;
    	this.iva = iva;
    	this.mensaje = mensaje;
    	this.operador = operador;
    	this.precio = precio;
    	this.premio = premio;
    	this.principalLocal = principalLocal;
    	this.principalPesos = principalPesos;
    	this.redondeo = redondeo;
    	this.referencia1 = referencia1;
    	this.referencia2 = referencia2;
    	this.referencia3 = referencia3;
    	this.subTotal = subTotal;
    	this.terminal = terminal;
    	this.tienda = tienda;
    	this.tipoCambio = tipoCambio;
    	this.tipoMoneda = tipoMoneda;
    	this.total = total;
    }
	
	
//	public PagoServicioVO(String clave,String disponible1,String disponible2,String disponible3,String fechaOperacion,String folioCliente,String folioProveedor,String folioTelecomm,String idProductoTerceros,String iva,String mensaje,String operador,String precio,String premio,String principalLocal,String principalPesos,String redondeo,String referencia1,String referencia2,String referencia3,String subTotal,String terminal,String tienda,String tipoCambio,String tipoMoneda,String total,long idUsuario,String imei,String software,String modelo,String wkey,int pase) {
//    	
//    	this.clave = clave;
//    	this.disponible1 = disponible1;
//    	this.disponible2 = disponible2;
//    	this.disponible3 = disponible3;
//    	this.fechaOperacion = fechaOperacion;
//    	this.folioCliente = folioCliente;
//    	this.folioProveedor = folioProveedor;
//    	this.folioTelecomm = folioTelecomm;
//    	this.idProductoTerceros = idProductoTerceros;
//    	this.iva = iva;
//    	this.mensaje = mensaje;
//    	this.operador = operador;
//    	this.precio = precio;
//    	this.premio = premio;
//    	this.principalLocal = principalLocal;
//    	this.principalPesos = principalPesos;
//    	this.redondeo = redondeo;
//    	this.referencia1 = referencia1;
//    	this.referencia2 = referencia2;
//    	this.referencia3 = referencia3;
//    	this.subTotal = subTotal;
//    	this.terminal = terminal;
//    	this.tienda = tienda;
//    	this.tipoCambio = tipoCambio;
//    	this.tipoMoneda = tipoMoneda;
//    	this.total = total;
//    	this.idUsuario = idUsuario;
//    	this.imei = imei;
//    	this.software = software;
//    	this.modelo = modelo;
//    	this.wkey = wkey;
//    	this.pase = pase;    	 
//    	
//    }

    
    public PagoServicioVO() {
    	
    }
    
    
    public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getDisponible1() {
		return disponible1;
	}
	public void setDisponible1(String disponible1) {
		this.disponible1 = disponible1;
	}
	public String getDisponible2() {
		return disponible2;
	}
	public void setDisponible2(String disponible2) {
		this.disponible2 = disponible2;
	}
	public String getDisponible3() {
		return disponible3;
	}
	public void setDisponible3(String disponible3) {
		this.disponible3 = disponible3;
	}
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	public String getFolioCliente() {
		return folioCliente;
	}
	public void setFolioCliente(String folioCliente) {
		this.folioCliente = folioCliente;
	}
	public String getFolioProveedor() {
		return folioProveedor;
	}
	public void setFolioProveedor(String folioProveedor) {
		this.folioProveedor = folioProveedor;
	}
	public String getFolioTelecomm() {
		return folioTelecomm;
	}
	public void setFolioTelecomm(String folioTelecomm) {
		this.folioTelecomm = folioTelecomm;
	}
	public String getIdProductoTerceros() {
		return idProductoTerceros;
	}
	public void setIdProductoTerceros(String idProductoTerceros) {
		this.idProductoTerceros = idProductoTerceros;
	}
	public String getIva() {
		return iva;
	}
	public void setIva(String iva) {
		this.iva = iva;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getOperador() {
		return operador;
	}
	public void setOperador(String operador) {
		this.operador = operador;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	public String getPremio() {
		return premio;
	}
	public void setPremio(String premio) {
		this.premio = premio;
	}
	public String getPrincipalLocal() {
		return principalLocal;
	}
	public void setPrincipalLocal(String principalLocal) {
		this.principalLocal = principalLocal;
	}
	public String getPrincipalPesos() {
		return principalPesos;
	}
	public void setPrincipalPesos(String principalPesos) {
		this.principalPesos = principalPesos;
	}
	public String getRedondeo() {
		return redondeo;
	}
	public void setRedondeo(String redondeo) {
		this.redondeo = redondeo;
	}
	public String getReferencia1() {
		return referencia1;
	}
	public void setReferencia1(String referencia1) {
		this.referencia1 = referencia1;
	}
	public String getReferencia2() {
		return referencia2;
	}
	public void setReferencia2(String referencia2) {
		this.referencia2 = referencia2;
	}
	public String getReferencia3() {
		return referencia3;
	}
	public void setReferencia3(String referencia3) {
		this.referencia3 = referencia3;
	}
	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public String getTienda() {
		return tienda;
	}
	public void setTienda(String tienda) {
		this.tienda = tienda;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}

    public boolean isConsultaWS() {
		return consultaWS;
	}
	public void setConsultaWS(boolean consultaWS) {
		this.consultaWS = consultaWS;
	}
	
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	 public long getIdUsuario() {
			return idUsuario;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getWkey() {
		return wkey;
	}

	public void setWkey(String wkey) {
		this.wkey = wkey;
	}

	public int getPase() {
		return pase;
	}

	public void setPase(int pase) {
		this.pase = pase;
	}

	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidoPaterno() {
		return apellidoPaterno;
	}


	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}


	public String getApellidoMaterno() {
		return apellidoMaterno;
	}


	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	
    public String getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	public String getRfc() {
		return rfc;
	}


	public void setRfc(String rfc) {
		this.rfc = rfc;
	}


	public String getCalle() {
		return calle;
	}


	public void setCalle(String calle) {
		this.calle = calle;
	}


	public String getColonia() {
		return colonia;
	}


	public void setColonia(String colonia) {
		this.colonia = colonia;
	}


	public String getCiudad() {
		return ciudad;
	}


	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getCodigoPostal() {
		return codigoPostal;
	}


	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
