package com.addcel.telecom.pago.conciliacion.servicios.job;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.addcel.telecom.pago.conciliacion.servicios.model.mapper.ServiceMapper;
import com.addcel.telecom.pago.conciliacion.servicios.model.vo.PagoServicioVO;
import com.addcel.telecom.pago.conciliacion.servicios.utils.UtilsServices;




public class TelecomPagoServicioCron implements TelecomPagoServicio {
	
	@Autowired
	private ServiceMapper mapper;

	private static final Logger LOGGER = LoggerFactory.getLogger(TelecomPagoServicioCron.class);

	public void work() {
		String threadName = Thread.currentThread().getName();
		LOGGER.info("   " + threadName + " has began working.");
        try {
        	LOGGER.info("working...");
            Thread.sleep(10000); // simulates work
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        LOGGER.info("   " + threadName + " has completed work.");
	}	

	
	public List<PagoServicioVO> consultarPagosServicioTelecom()	{
		
		LOGGER.info("consultarPagosServicioTelecom");
		List<PagoServicioVO> listaPagoServicioVO  = mapper.consultarPagoServicioTelecom();
		LOGGER.info("listaPagoServicioVO "+listaPagoServicioVO);
		LOGGER.info("longitud de la lista: "+listaPagoServicioVO.size());		
		return listaPagoServicioVO;		
	}
	
	public void procesarCociliacion() {	
		
		try {
			LOGGER.info("Inicio procesarCociliacion");		
			List<PagoServicioVO> listaPagoServicioVo = consultarPagosServicioTelecom();		
			UtilsServices utilsServices = new UtilsServices();
			utilsServices.crearArchivo(listaPagoServicioVo);
			utilsServices.SFTPConnection();			
			utilsServices.eliminarFichero();
			LOGGER.info("Finalizacion del procesarCociliacion");
		}
        catch (Exception e) {        	
            Thread.currentThread().interrupt();
            LOGGER.error("ERROR procesarCociliacion: "+e);
        }
		
	}
	
	public void procesarCociliacion2() {
		
		String threadName = Thread.currentThread().getName();
		LOGGER.info("   " + threadName + " has began working.");
        try {
        	LOGGER.info("working...");
            Thread.sleep(10000); // simulates work
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        LOGGER.info("   " + threadName + " has completed work.");
		
	}
	
	
}
