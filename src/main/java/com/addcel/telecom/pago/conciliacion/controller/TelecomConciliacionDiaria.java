package com.addcel.telecom.pago.conciliacion.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class TelecomConciliacionDiaria {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TelecomConciliacionDiaria.class);
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public void getCommonPage() {
    	LOGGER.info("Received request to show welcome page");
	}

}
