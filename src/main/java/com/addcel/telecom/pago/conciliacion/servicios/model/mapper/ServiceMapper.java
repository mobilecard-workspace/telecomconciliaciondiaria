package com.addcel.telecom.pago.conciliacion.servicios.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.addcel.telecom.pago.conciliacion.servicios.model.vo.PagoServicioVO;




public interface ServiceMapper {	
	
	public int insertBitacoraPagoServicio(PagoServicioVO pagoServicioVO);
	public List<PagoServicioVO> consultarPagoServicioTelecom();

}
