package com.addcel.telecom.pago.conciliacion.servicios.job;

import java.util.List;

import com.addcel.telecom.pago.conciliacion.servicios.model.vo.PagoServicioVO;

public interface TelecomPagoServicio {
	
	public void work();
	
	public List<PagoServicioVO> consultarPagosServicioTelecom();
	
	public void procesarCociliacion();
	
}
