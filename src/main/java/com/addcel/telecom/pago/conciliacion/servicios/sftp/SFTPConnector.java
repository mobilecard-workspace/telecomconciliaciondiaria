package com.addcel.telecom.pago.conciliacion.servicios.sftp;

import com.addcel.telecom.pago.conciliacion.servicios.utils.UtilsServices;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
/**
 * Clase encargada de establecer conexion y ejecutar comandos SFTP.
 */
public class SFTPConnector {
 
    /**
     * Sesion SFTP establecida.
     */
    private Session session;
 
    private static final Logger LOGGER = LoggerFactory.getLogger(SFTPConnector.class);
    /**
     * Establece una conexion SFTP.
     *
     * @param username Nombre de usuario.
     * @param password Contrasena.
     * @param host     Host a conectar.
     * @param port     Puerto del Host.
     *
     * @throws JSchException          Cualquier error al establecer
     *                                conexión SFTP.
     * @throws IllegalAccessException Indica que ya existe una conexion
     *                                SFTP establecida.
     */
    public void connect(String username, String password, String host, int port)
        throws JSchException, IllegalAccessException {
        if (this.session == null || !this.session.isConnected()) {
            JSch jsch = new JSch();
            System.out.println("connect1");
            this.session = jsch.getSession(username, host, port);
            System.out.println("connect2");
            this.session.setPassword(password);
            System.out.println("connect3");
            // Parametro para no validar key de conexion.
            this.session.setConfig("StrictHostKeyChecking", "no");
            System.out.println("connect4"); 
            this.session.connect();
        } else {
        	System.out.println("connect5");
            throw new IllegalAccessException("Sesion SFTP ya iniciada.");
        }
    }
 
    /**
     * Añade un archivo al directorio FTP usando el protocolo SFTP.
     *
     * @param ftpPath  Path del FTP donde se agregará el archivo.
     * @param filePath Directorio donde se encuentra el archivo a subir en
     *                 disco.
     * @param fileName Nombre que tendra el archivo en el destino.
     *
     * @throws IllegalAccessException Excepción lanzada cuando no hay
     *                                conexión establecida.
     * @throws JSchException          Excepción lanzada por algún
     *                                error en la ejecución del comando
     *                                SFTP.
     * @throws SftpException          Error al utilizar comandos SFTP.
     * @throws IOException            Excepción al leer el texto arrojado
     *                                luego de la ejecución del comando
     *                                SFTP.
     */
    public final void addFile(String ftpPath, String filePath,
        String fileName) throws IllegalAccessException, IOException,
        SftpException, JSchException {
    	
    	try	{
	        if (this.session != null && this.session.isConnected()) {
	 
	            // Abrimos un canal SFTP. Es como abrir una consola.
	            ChannelSftp channelSftp = (ChannelSftp) this.session.
	                openChannel("sftp");
	            System.out.println("addFile1");
	            // Nos ubicamos en el directorio del FTP.
	            //channelSftp.cd("/home/jsegovia/");
	            System.out.println("addFile2");
	            channelSftp.connect();
	            System.out.println("addFile3");
	 
	            System.out.println(String.format("Creando archivo %s en el " +
	                "directorio %s", fileName, ftpPath));
	            channelSftp.put(filePath, fileName);
	 
	            System.out.println("Archivo subido exitosamente");
	 
	            channelSftp.exit();
	            channelSftp.disconnect();
	        } else {
	            throw new IllegalAccessException("No existe sesion SFTP iniciada.");
	        }
	   	}
    	catch(Exception e)	
    	{
    		LOGGER.error("Error SFTP: "+e);
    	}
    }
 
    /**
     * Cierra la sesion SFTP.
     */
    public final void disconnect() {
        this.session.disconnect();
    }
}
