package com.addcel.telecom.pago.conciliacion.servicios.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.telecom.pago.conciliacion.servicios.model.vo.PagoServicioVO;
import com.addcel.telecom.pago.conciliacion.servicios.sftp.SFTPConnector;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;


public class UtilsServices {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilsServices.class);
	private String nombreArchivo = getNombreArchivo();
	String ruta = "/home/mauricio/workspace_telecom/conciliacion/"+nombreArchivo;	
	List<PagoServicioVO> listaPagoServicioVo;
	
public void crearArchivo(List<PagoServicioVO> listaPago)	{		
				
		File archivo = new File(ruta);		
		String linea;
		BufferedWriter bw;
		FileWriter fw = null;
		
		
		try {
			/*if(archivo.exists()) {
			      // El fichero ya existe
				 bw = new BufferedWriter(new FileWriter(archivo));
				 //bw.append("fichero existe");
			} else {*/
					
				   listaPagoServicioVo = listaPago;
				   fw = new FileWriter(ruta, true);
				   bw = new BufferedWriter(fw);
				   crearCuerpoArchivo(bw);
				   bw.close();				   
		//}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error crearArchivo: "+e);
		}
		
	} 
	
	public String crearLinea(int consecutivo,PagoServicioVO pagoServicioVo)	{
		
		String linea="";		
		linea =  consecutivo+"|"+pagoServicioVo.getFolioTelecomm()+"|"+Constantes.CODIGO_AGENTE+"|"+pagoServicioVo.getTienda()+"|";
		linea += pagoServicioVo.getTerminal()+"|"+pagoServicioVo.getOperador()+"|"+pagoServicioVo.getIdProductoTerceros()+"|";
		linea += pagoServicioVo.getFolioCliente()+"|"+pagoServicioVo.getFolioProveedor()+"|"+pagoServicioVo.getReferencia1()+"|";
		linea += pagoServicioVo.getReferencia2()+"|"+pagoServicioVo.getReferencia3()+"|"+pagoServicioVo.getFechaOperacion()+"|"+pagoServicioVo.getPrincipalPesos()+"|";
		linea += pagoServicioVo.getPremio()+"|"+pagoServicioVo.getIva()+"|"+pagoServicioVo.getPrecio()+"|"+pagoServicioVo.getSubTotal()+"|";
		linea += pagoServicioVo.getRedondeo()+"|"+pagoServicioVo.getTotal()+"|"+pagoServicioVo.getNombre()+"|"+pagoServicioVo.getApellidoPaterno()+"|";
		linea += pagoServicioVo.getApellidoMaterno()+"|"+pagoServicioVo.getFechaNacimiento()+"|";
		linea += pagoServicioVo.getRfc()+"|"+pagoServicioVo.getCalle()+"|"+pagoServicioVo.getColonia()+"|"+pagoServicioVo.getCiudad()+"|";
		linea += pagoServicioVo.getEstado()+"|"+pagoServicioVo.getCodigoPostal()+"|"+pagoServicioVo.getTelefono();
		return linea;
		
	}
	
	public void crearCuerpoArchivo(BufferedWriter bw) throws IOException	{
		
		String linea="";
		int consecutivo = 0;	
		
		/*float totalPrincipalPesos = 0.00F;
		float totalComision = 0.00F;
		float totalIva = 0.00F;
		float totalRedondeo = 0.00F;*/
		double totalPrincipalPesos = 0.00D;
		double totalComision = 0.00D;
		double totalIva = 0.00D;
		double totalRedondeo = 0.00D;
		bw.newLine();
		for(PagoServicioVO pagoServicioVO : listaPagoServicioVo)	{
			++consecutivo;
			linea = crearLinea(consecutivo,pagoServicioVO);
			bw.write(linea);
			bw.newLine();
			LOGGER.info("crearCuerpoArchivo1: ");			 
			totalPrincipalPesos += convertToFloat(pagoServicioVO.getPrincipalPesos());
			LOGGER.info("crearCuerpoArchivo2: ");
			totalComision += convertToFloat(pagoServicioVO.getPremio());
			LOGGER.info("crearCuerpoArchivo3: ");
			totalIva += convertToFloat(pagoServicioVO.getIva());
			LOGGER.info("crearCuerpoArchivo4: ");
			totalRedondeo += convertToFloat(pagoServicioVO.getRedondeo());
			LOGGER.info("crearCuerpoArchivo5: ");
		} 
		
		
		if (listaPagoServicioVo.size() > 0)	{
			crearEncabezado(totalPrincipalPesos,totalComision,totalIva,totalRedondeo);
			crearPiePagina(bw,totalPrincipalPesos,totalComision,totalIva,totalRedondeo);
		}
		else {
			LOGGER.info("El archivo esta vacio");
			
		}
	}
	
	public String redondearNumero(double floatNumber)	{		

		
		
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        DecimalFormatSymbols dfs = decimalFormat.getDecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        decimalFormat.setDecimalFormatSymbols(dfs);
        String numberAsString = decimalFormat.format(floatNumber);
        
        LOGGER.info("numberAsString: "+numberAsString);
		return numberAsString;
		
	}
	
	public void crearEncabezado(double totalPrincipalPesos,double totalComision,double totalIva, double totalRedondeo)	{
		
		String encabezadoPieArchivo;
		
		try	{
			RandomAccessFile f = new RandomAccessFile(new File(ruta), "rw");
			f.seek(0); // to the beginning
			encabezadoPieArchivo = encabezadoPieArchivo(listaPagoServicioVo.size(),Double.toString(totalPrincipalPesos),Double.toString(totalComision),Double.toString(totalIva),Double.toString(totalRedondeo));
			f.write(encabezadoPieArchivo.getBytes());		
			f.close();
		}catch (IOException e) {
			LOGGER.error("Error crearEncabezado: "+e);
		}
		
	}
	
	public void  crearPiePagina(BufferedWriter bw,double totalPrincipalPesos,double totalComision,double totalIva, double totalRedondeo)	{
		
		String lineaPieArchivo;
		try {
			lineaPieArchivo = encabezadoPieArchivo(listaPagoServicioVo.size(),Double.toString(totalPrincipalPesos),Double.toString(totalComision),Double.toString(totalIva),Double.toString(totalRedondeo));		
			bw.write(lineaPieArchivo);
			bw.newLine();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.info("Error crearPiePagina: "+e);
		}
		
		
	}
	
	public double convertToFloat(String number)	{
		
		LOGGER.info("number: "+number);
		float floatNumber = 0.00F;
		double doubleNumber = 0.00D;
		
		if ((number != null) && (!number.equals("")))	{
			doubleNumber = Double.parseDouble(number);
			number = redondearNumero(doubleNumber);
			doubleNumber = Double.parseDouble(number);
		} 
		
		return doubleNumber;
	}
	
	public String encabezadoPieArchivo(int registros,String totalPrincipalPesos,String totalComision, String totalIva, String totalRedondeo)	{
		
		String linea;
		String fecha = fechaAyer();
		linea = Constantes.CODIGO_AGENTE+"|"+fecha+"|"+registros+"|"+totalPrincipalPesos+"|"+totalComision+"|"+totalIva+"|"+totalRedondeo;
		return linea;
	}
	public String getNombreArchivo()	{
		
		String nombreArchivo;
      	Calendar cal = Calendar.getInstance();
      	Instant ayer = Instant.now().minus(1,ChronoUnit.DAYS);      	
      	cal.setTime(Date.from(ayer));      	
	    String result = String.format("%1$tY%1$tm%1$td", cal);
	    LOGGER.info("result: "+result);
	    int numero = 2002;	    
	    Formatter condigoAgenteFmt = new Formatter();
	    condigoAgenteFmt.format("%010d",numero);
	    LOGGER.info("condigoAgenteFmt: "+condigoAgenteFmt);
	    nombreArchivo = condigoAgenteFmt+result+".txt";
	    LOGGER.info("nombreArchivo: "+nombreArchivo);
	    return nombreArchivo;

	}
	
	public void SFTPConnection()	{
		
		try {
            SFTPConnector sshConnector = new SFTPConnector();
             
             
            sshConnector.connect(Constantes.SFTP_USERNAME,Constantes.SFTP_PASSWORD, Constantes.SFTP_HOST, Constantes.SFTP_PORT);
            sshConnector.addFile("/home/jsegovia/", ruta,nombreArchivo);
            //sshConnector.addFile("/", ruta,nombreArchivo);
            sshConnector.disconnect();
        } catch (JSchException ex) {
            ex.printStackTrace();
             
            System.out.println(ex.getMessage());
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
             
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            ex.printStackTrace();
             
            System.out.println(ex.getMessage());
        } catch (SftpException ex) {
            ex.printStackTrace();
             
            System.out.println(ex.getMessage());
        }
		
	}
	
	public String fechaAyer()	{
		
		Calendar cal = Calendar.getInstance();
      	Instant ayer = Instant.now().minus(1,ChronoUnit.DAYS);      	
      	cal.setTime(Date.from(ayer));      	
	    String result = String.format("%1$tY%1$tm%1$td", cal);
	    return result;
	}
	
	public void eliminarFichero() {

		LOGGER.info("eliminarFichero");
		
		File fichero = new File(ruta);
	    if (!fichero.exists()) {
	    	LOGGER.info("El archivo no existe.");	        
	    } else {
	        fichero.delete();	        
	        LOGGER.info("El archivo data fue eliminado.");
	    }

	}
}
